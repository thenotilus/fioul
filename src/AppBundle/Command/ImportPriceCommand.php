<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use AppBundle\Services\FioulImportInterface;

class ImportPriceCommand extends ContainerAwareCommand
{

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('import:price:csv')
            ->setDescription('Import Price from CSV file')
            ->addArgument('path', InputArgument::OPTIONAL, 'Give path to csv')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $start = new \DateTime('now');
        $output->writeln('<comment>Start : ' . $start->format('d-m-Y G:i:s') . '</comment>');

        $this->import($output, $input->getArgument('path'));

        $end = new \DateTime('now');
        $output->writeln('<comment>End : ' . $end->format('d-m-Y G:i:s') . '</comment>');
    }

    protected function import(OutputInterface $output, $path = null)
    {
        /** @var FioulImportInterface $fioulManager */
        $fioulManager = $this->getContainer()->get('fioul.services');

        $fioulManager->importFromCsv($path);
    }
}