<?php

namespace AppBundle\Components;

/**
 * Class AbstractFile
 */
class AbstractFile
{
    /** @var String */
    protected $filePath;

    /** @var string */
    protected $delimiter;

    /** @var string */
    protected $enclosure;

    /** @var string */
    protected $escape;

    /** @var resource File pointer */
    protected $file = null;

    /**
     * Reader constructor.
     *
     * @param string  $filePath Path to the csv file.
     * @param string $delimiter Delimiter to use for the csv file.
     * @param string $enclosure Enclosure to use for the csv file.
     * @param string $escape    Escape to use for the csv file.
     */
    public function __construct($filePath, $delimiter = ',', $enclosure = "'", $escape = '\\')
    {
        $this->filePath = $filePath;
        $this->delimiter = $delimiter;
        $this->enclosure = $enclosure;
        $this->escape = $escape;
    }
}
