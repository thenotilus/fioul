<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Fioul;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations as Rest;

class FioulController extends Controller
{
    /**
     * @Rest\Get(
     *     path = "/fiouls/{id}/{fdate}/{sdate}",
     *     name = "app_fiouls_show_between_dates",
     *     requirements = {
     *          "id"="\d+",
     *          "fdate" = "[0-9-]{10}",
     *          "sdate" = "[0-9-]{10}"
     *      }
     * )
     * @View
     */
    public function getFioulsBetweenDatesAction(Request $request)
    {
        return $this->getDoctrine()
            ->getRepository(Fioul::class)
            ->getFiouls($request->attributes->get('id'),$request->attributes->get('fdate'),$request->attributes->get('sdate'));
    }

}
