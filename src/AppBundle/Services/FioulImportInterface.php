<?php

namespace AppBundle\Services;

interface FioulImportInterface
{

    /**
     * @param string $path
     *
     * @return mixed
     */
    public function importFromCsv($path = null);

    /**
     * @param $importCsv
     *
     * @return mixed
     */
    public function prepareMysqlRequest($importCsv);

    /**
     * @return mixed
     */
    public function readFileMysqlRequest();

    /**
     * @return mixed
     */
    public function save();
}