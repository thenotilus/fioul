<?php

namespace AppBundle\Services;

use AppBundle\Components\Reader;

class FioulImport implements FioulImportInterface
{
    /**
     * @var array $fieldsImport
     */
    protected $fieldsImport;
    /**
     * @var \PDO $pdo
     */
    protected $pdo;
    protected $defaultFilePath;

    protected $query;
    protected $rootDirProject;

    /**
     * FioulManager constructor.
     *
     * @param       $rootDirProject
     * @param       $defaultFilePath
     * @param array $fieldsImport
     * @param \PDO  $pdo
     */
    public function __construct($rootDirProject,$defaultFilePath, $fieldsImport = [], \PDO $pdo)
    {
        $this->rootDirProject   = $rootDirProject;
        $this->defaultFilePath  = $defaultFilePath;
        $this->fieldsImport     = $fieldsImport;
        $this->pdo              = $pdo;
    }

    /**
     * @inheritdoc
     */
    public function importFromCsv($path = null)
    {
        if(is_null($path)){
            $importCsv = new Reader($this->defaultFilePath);
        } else {
            $importCsv = new Reader($this->rootDirProject.$path);
        }

        $this->prepareMysqlRequest($importCsv);
        $this->save();
    }

    /**
     * @inheritdoc
     */
    public function prepareMysqlRequest($importCsv)
    {
        $firstElement   = true;
        $lastElement    = false;
        $i              = 0;

        if (!file_exists("/tmp/importIntoBdd.sql"))
        {
            while (!$lastElement){
                foreach ($importCsv as $items) {
                    if ($firstElement){
                        $sql = "(''," . implode(",", $items) . ")";
                        $firstElement = false;
                    }
                    $sql .= ",(''," . implode(",", $items) . ")";
                }
                if ($i%5000===0){
                    $handle = fopen("/tmp/importIntoBdd.sql", "x+");
                    fputs($handle,$sql);
                    fclose($handle);
                    $sql = null;
                }
                if (is_null($sql))
                    $lastElement = true;
                $i++;
            }
        }
        $this->readFileMysqlRequest();
    }

    /**
     * @inheritdoc
     */
    public function readFileMysqlRequest()
    {
        if (file_exists("/tmp/importIntoBdd.sql")) {
            $handle = fopen("/tmp/importIntoBdd.sql", "r");
            $this->query = fgets($handle);
            fclose($handle);
        }
    }

    /**
     * @inheritdoc
     */
    public function save()
    {
        if (!is_null($this->query))
            $this->pdo->query('INSERT INTO `fioul`(`id`, `postal_code_id`, `amount`, `date`) VALUES ' .$this->query);
    }
}